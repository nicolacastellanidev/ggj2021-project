extends StateMachineState
class_name CompleteRequestState

func enter(old_state = null) -> void:
	print("[COMPLETE_REQUEST] Enter")
	var request_id = GameManager._args_request_id
	assert(request_id > -1 and GameManager._requests.has(request_id))
	
	var request = GameManager._requests[request_id]
	
	var completed_room_id = GameManager._categories_to_rooms[request.item]
	var completed_room = GameManager._rooms[completed_room_id]
	completed_room.instance.spawn_items()
	
	GameManager._remove_timer(request.timeout_timer)
	GameManager._add_points(request.reward)
	GameManager._requests.erase(request_id)
	
	GameManager._current_request_id = GameManager._requests.keys()[0] if GameManager._requests.size() > 0 else - 1
	
	if GameManager._current_request_id > -1:
		print("[COMPLETE_REQUEST] Go to the next request (req: %d)" % GameManager._current_request_id)


func run(delta: float):
	return Constants.GameStates.EXEC_REQUESTS if GameManager._current_request_id > -1 else Constants.GameStates.IDLE


func exit() -> void:
	print("[COMPLETE_REQUEST] Exit")
	print("[COMPLETE_REQUEST] Remaining requests: %d" % GameManager._requests.size())
	print("[COMPLETE_REQUEST] Current score: %d" % GameManager._points)
	
	GameManager._dialogue.open_message(Constants.Messages.REQUEST_COMPLETED)
	GameManager._forward_signal("request_failed")
