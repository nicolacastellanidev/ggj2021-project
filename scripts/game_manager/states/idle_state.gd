extends StateMachineState
class_name IdleState

var _first_request_timer: int

func enter(old_state = null) -> void:
	print("[IDLE] Enter")
	var random_time: int = GameManager._rng.randi_range(Constants.Time_FIRST_REQUEST_DELAY_MIN, Constants.Time_FIRST_REQUEST_DELAY_MAX)
	print("[IDLE] First request in %d seconds..." % random_time)
	_first_request_timer = GameManager._append_timer("incoming_request_0", random_time, self, "_start_first_request")

func run(delta: float):
	#print("[IDLE STATE] Run")
	return null

func exit() -> void:
	print("[IDLE] Exit")

func _start_first_request() -> void:
	GameManager._remove_timer(_first_request_timer)
	GameManager.new_request()
