extends StateMachineState
class_name EndState

func enter(old_state = null) -> void:
	print("[END] Enter")

	for timer in GameManager._timers:
		timer.stop()

	GameManager._forward_signal("round_ended")
	
func run(delta: float):
	return null

func exit() -> void:
	pass
