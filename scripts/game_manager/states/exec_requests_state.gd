extends StateMachineState
class_name ExecRequestsState

func enter(old_state = null) -> void:
	print("[EXEC_REQUESTS] Enter")
	pass

func run(delta: float):
	return null

func exit() -> void:
	print("[EXEC_REQUESTS] Exit")
	pass
