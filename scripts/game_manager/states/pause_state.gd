extends StateMachineState
class_name PauseState

var _just_entered: bool
var _previous_state = null

func enter(old_state = null) -> void:
	print("[PAUSE_STATE] Enter")
	get_tree().paused = true
	_previous_state = old_state
	_just_entered = true


func run(delta: float):
	# TODO - Connect to UI buttons
	if Input.is_action_just_pressed(Constants.Input_PAUSE) and not _just_entered:
		return _previous_state if _previous_state != null else Constants.GameStates.IDLE
	
	_just_entered = false
	return null


func exit() -> void:
	print("[PAUSE_STATE] Exit")
	get_tree().paused = false
