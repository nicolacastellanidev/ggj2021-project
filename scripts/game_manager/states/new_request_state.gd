extends StateMachineState
class_name NewRequestState

var _next_request_timer: int
var _failed_request_timer: int

func enter(old_state = null) -> void:
	print("[NEW_REQUEST] Enter")

	var request = Constants.ItemRequest.new()
	request.id = GameManager._generate_request_id()
	request.reward = Constants.Points_COMPLETED
	
	var random_type_index = GameManager._rng.randi_range(0, Constants.ItemCategories.size() - 1)
	var random_type_name: String = Constants.ItemCategories.keys()[random_type_index]
	request.item = Constants.ItemCategories[random_type_name]
	
	var random_time: int = GameManager._rng.randi_range(Constants.Time_TIMEOUT_REQUEST_MIN, Constants.Time_TIMEOUT_REQUEST_MAX)
	request.timeout_timer = GameManager._append_timer(("timeout_request_%d" % request.id), random_time, GameManager, "fail_request", [request.id])
	GameManager._requests[request.id] = request
	
	if GameManager._requests.size() == 1:
		GameManager._current_request_id = request.id
		print("[NEW_REQUEST] You have a new goal (req. %d): try to collect %s in %d seconds. Good luck!" % [request.id, random_type_name, random_time])
	else:
		print("[NEW_REQUEST] Append a new request for later (req. %d: %s in %d seconds)" % [request.id, random_type_name, random_time])

	random_time = GameManager._rng.randi_range(Constants.Time_NEXT_REQUEST_DELAY_MIN, Constants.Time_NEXT_REQUEST_DELAY_MAX)
	var next_request_id = request.id + 1
	var next_request_timer = GameManager._append_timer(("incoming_request_%d" % next_request_id), random_time, GameManager, "new_request")
	print("[NEW_REQUEST] Next request in %d seconds..." % random_time)

func run(delta: float):
	return Constants.GameStates.EXEC_REQUESTS

func exit() -> void:
	print("[NEW_REQUEST] Exit")
	GameManager._forward_signal("request_accepted")
