extends StateMachineState
class_name StartState

var _countdown_ended: bool
var _countdown_timer: int

func enter(old_state = null) -> void:
	print("[START] Enter")

	GameManager._points = 0
	GameManager._requests = {}
	GameManager._last_request_id = -1
	GameManager._current_request_id = -1
	
	for timer in GameManager._timers:
		timer.queue_free()

	var game_duration: int = Constants.Time_WORKING_HOURS * 60 * Constants.Time_WMIN_TO_RSEC
	GameManager._append_timer("round_time", game_duration, GameManager, "end_game")

	_countdown_ended = false
	_countdown_timer = GameManager._append_timer("starting_countdown", Constants.Time_INITIAL_COUNTDOWN, self, "_stop_countdown")


func run(delta: float):
	return Constants.GameStates.IDLE if _countdown_ended else null


func exit() -> void:
	print("[START] Exit")
	GameManager._dialogue.start_conversation(Constants.Conversations.WELCOME)
	GameManager._forward_signal("round_started")


func _stop_countdown() -> void:
	_countdown_ended = true
	GameManager._remove_timer(_countdown_timer)
