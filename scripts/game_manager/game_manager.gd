extends Node

signal round_started
signal round_ended
signal points_changed
signal request_accepted
signal request_completed
signal request_failed

var _points: int

var _sm: StateMachine

var _timers: Array
var _timers_node: Node

var _rng: RandomNumberGenerator

var _requests: Dictionary
var _last_request_id: int
var _current_request_id: int

var _args_request_id: int

var _rooms: Dictionary = {}
var _rooms_node: Node2D
var _categories_to_rooms: Dictionary = {}
var _old_room
var _current_room

var _player: Node2D
var _hud: Control
var _main_menu: Control
var _pause_menu: Control
var _dialogue: Node

func register_player(instance):
	_player = instance

func register_hud(instance):
	_hud = instance

func register_main_menu(instance):
	_main_menu = instance

func register_pause_menu(instance):
	_pause_menu = instance

func _init() -> void:
	_sm = StateMachine.new()
	_sm.name = "states"
	add_child(_sm)
	
	_timers_node = Node.new()
	_timers_node.name = "timers"
	add_child(_timers_node)

	_rng = RandomNumberGenerator.new()

	pause_mode = Node.PAUSE_MODE_PROCESS
	_timers_node.pause_mode = Node.PAUSE_MODE_STOP

	_dialogue = DialogueManager.new()
	add_child(_dialogue)


func _ready() -> void:
	if _player == null:
		_player = get_node("/root/main/Player")
	_rooms_node = get_node("/root/main/rooms")
	if _rooms_node == null:
		return
	
	_sm.add_state(Constants.GameStates.START, StartState, "start")
	_sm.add_state(Constants.GameStates.IDLE, IdleState, "idle")
	_sm.add_state(Constants.GameStates.NEW_REQUEST, NewRequestState, "new_request")
	_sm.add_state(Constants.GameStates.EXEC_REQUESTS, ExecRequestsState, "exec_requests")
	_sm.add_state(Constants.GameStates.COMPLETE_REQUEST, CompleteRequestState, "complete_request")
	_sm.add_state(Constants.GameStates.FAIL_REQUEST, FailRequestState, "fail_request")
	_sm.add_state(Constants.GameStates.PAUSE, PauseState, "pause_request")
	_sm.add_state(Constants.GameStates.END, EndState, "end")

	_rng.randomize()
	
	_add_room(Constants.RoomTypes.OFFICE, null, null, Constants.RoomTypes.THEME_1, null, null)
	_add_room(Constants.RoomTypes.THEME_1, Constants.ItemCategories.THEME_1, Constants.RoomTypes.THEME_2, Constants.RoomTypes.THEME_3, Constants.RoomTypes.THEME_4, Constants.RoomTypes.OFFICE)
	_add_room(Constants.RoomTypes.THEME_2, Constants.ItemCategories.THEME_2, null, null, Constants.RoomTypes.THEME_1, null)
	_add_room(Constants.RoomTypes.THEME_3, Constants.ItemCategories.THEME_3, null, null, null, Constants.RoomTypes.THEME_1)
	_add_room(Constants.RoomTypes.THEME_4, Constants.ItemCategories.THEME_4, Constants.RoomTypes.THEME_1, null, null, null)
	
	_old_room = null
	_current_room = null
	change_room(null, null)

	#TODO Remove when GUI menu is available
	new_game()


func _add_room(index, category, left, top, right, bottom):
	var room = Constants.Room.new()
	room.id = index
	room.name = Constants.RoomTypes.keys()[index]
	room.items_category = category
	room.left_door = left
	room.top_door = top
	room.right_door = right
	room.bottom_door = bottom
	
	match index:
		Constants.RoomTypes.OFFICE:
			var office_scene = preload("res://scenes/rooms/office.tscn")
			room.instance = office_scene.instance()
		
		Constants.RoomTypes.THEME_1:
			var room_scene = preload("res://scenes/rooms/world_1.tscn")
			room.instance = room_scene.instance()
			room.instance.setup(index, category)

		Constants.RoomTypes.THEME_2:
			var room_scene = preload("res://scenes/rooms/world_2.tscn")
			room.instance = room_scene.instance()
			room.instance.setup(index, category)
			
		Constants.RoomTypes.THEME_3:
			var room_scene = preload("res://scenes/rooms/world_3.tscn")
			room.instance = room_scene.instance()
			room.instance.setup(index, category)
			
		Constants.RoomTypes.THEME_4:
			var room_scene = preload("res://scenes/rooms/world_4.tscn")
			room.instance = room_scene.instance()
			room.instance.setup(index, category)
			
	room.instance.name = room.name
	_rooms[index] = room
	if room.items_category != null:
		_categories_to_rooms[room.items_category] = room.id

func _process(delta: float) -> void:
	if Input.is_action_just_pressed(Constants.Input_PAUSE):
		_sm.go_to(Constants.GameStates.PAUSE)


func _append_timer(name: String, time: float, target: Object, method: String, args: Array = []) -> int:
	assert(time > 0.0)
	
	var timer: = Timer.new()
	timer.name = name
	_timers_node.add_child(timer)
	_timers.append(timer)
	
	timer.connect("timeout", target, method, args)
	timer.start(time)

	var index: int = _timers.size() - 1
	return index


func _remove_timer(index: int) -> bool:
	if index >= _timers.size():
		return false

	var timer = _timers[index]
	assert(timer != null)
	timer.stop()
	
	# TODO - Reuse
	return true


func _forward_signal(name: String) -> void:
	emit_signal(name)


func _generate_request_id() -> int:
	_last_request_id = _last_request_id + 1
	return _last_request_id


func _add_points(value: int) -> void:
	_points = value + _points
	emit_signal("points_changed", _points)

func new_game() -> void:
	_sm.start(Constants.GameStates.START)

func end_game() -> void:
	_sm.go_to(Constants.GameStates.END)


func new_request() -> void:
	_sm.go_to(Constants.GameStates.NEW_REQUEST)


func complete_request(id: int) -> void:
	_args_request_id = id
	_sm.go_to(Constants.GameStates.COMPLETE_REQUEST)


func fail_request(id: int) -> void:
	_args_request_id = id
	_sm.go_to(Constants.GameStates.FAIL_REQUEST)

func get_elapsed_time() -> float:
	if(_timers == null): return 0.0
	return _timers[0].get_wait_time() - _timers[0].get_time_left()

func get_remaining_time() -> float:
	if(_timers == null): return 0.0
	return _timers[0].get_time_left()

func get_points() -> int:
	return _points

func give_item(item_type):
	var current_request = _requests[_current_request_id]
	assert(current_request != null)
	
	if item_type == current_request.item:
		complete_request(_current_request_id)
	else:
		fail_request(_current_request_id)


func change_room(from_room_id, from_door) -> void:
	var from_room = _rooms[from_room_id] if from_room_id != null else null
	
	var to_room_id
	var to_room
	var to_door
	
	if from_room == null or from_room.instance == null:
		print("[GAME_MGR] Restart from the first room")
		
		to_room_id = Constants.RoomTypes.OFFICE
		to_room = _rooms[to_room_id]
		to_door = null
	else:
		match from_door:
			Constants.DoorTypes.LEFT:
				to_room_id = from_room.left_door
				to_door = Constants.DoorTypes.RIGHT
			
			Constants.DoorTypes.TOP:
				to_room_id = from_room.top_door
				to_door = Constants.DoorTypes.BOTTOM
			
			Constants.DoorTypes.RIGHT:
				to_room_id = from_room.right_door
				to_door = Constants.DoorTypes.LEFT
			
			Constants.DoorTypes.BOTTOM:
				to_room_id = from_room.bottom_door
				to_door = Constants.DoorTypes.TOP
	
		to_room = _rooms[to_room_id] if to_room_id != null else null
		if to_room == null:
			print("[GAME_MGR] No target room for this door")
			return
	
		print("[GAME_MGR] Change from %s to %s" % [ from_room.name, to_room.name ])
	
	to_room.instance.on_room_enter(to_door)
	
	_old_room = _current_room
	_current_room = to_room.instance
	
	_rooms_node.call_deferred("add_child", _current_room)
	
	if _old_room != null:
		_rooms_node.call_deferred("remove_child", _old_room)
