extends Node2D

signal on_player_drop(item)

func _ready() -> void:
	pass # Replace with function body.


func _on_body_entered(body: Node) -> void:
	if body.is_in_group("player") && body.has_goal_item():
		var item = body.get_grabbed_item()
		if item != null:
			body.drop()
			emit_signal("on_player_drop", item)
