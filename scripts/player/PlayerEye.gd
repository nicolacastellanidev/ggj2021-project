extends Node2D

onready var body = $body

func _process(delta: float) -> void:
	var mouse_position = get_global_mouse_position()
	body.look_at(mouse_position)
	body.rotation = clamp(body.rotation, -PI/8, PI/8)
