extends Area2D

export var damage:float = 10

onready var body = $body
onready var collider = $collider
onready var animation = $animation
onready var body_original_position = body.position
onready var tween = $tween

var is_attacking = false

func _process(delta: float) -> void:
	var mouse_position = get_global_mouse_position()
	body.look_at(mouse_position)
	body.rotation = clamp(body.rotation, -PI/4, PI/4)
	
func attack() -> void:
	if tween.is_active(): tween.stop_all()
	var mouse_position = get_global_mouse_position()
	var angle = get_angle_to(mouse_position)
	var number = GameManager._rng.randi_range(25, 75)
	is_attacking = true;	
	body.position = body_original_position;
	tween.interpolate_property(body, "position",body.position,	Vector2(cos(angle), sin(angle)) * number, .1, Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	tween.interpolate_property(collider,"position",collider.position,Vector2(cos(angle), sin(angle)) * number, .1,Tween.TRANS_BACK, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_completed")
	_go_back()
	
func _go_back() -> void:
	is_attacking = false
	if tween.is_active(): tween.stop_all()
	tween.interpolate_property(body, "position",
	body.position, body_original_position, .25,
	Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	tween.interpolate_property(collider, "position",
	body.position, body_original_position, .25,
	Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	tween.start()
