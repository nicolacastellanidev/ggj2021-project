extends KinematicBody2D

# signals
signal on_stamina_change
signal on_die

# public vars
export var move_speed:int = 4 * 32
export var fire_rate:float = 0.1
export var max_stamina:float = 100
export var dodge_rate:float = 1

# private vars
var velocity:Vector2 = Vector2.ZERO
var move_direction:Vector2 = Vector2.ZERO
var desired_velocity:Vector2 = Vector2.ZERO

var cooldown:float = 0
var dodge_cooldown:float = 0
var punching_direction:int = 1 # 1 == right
var current_stamina:float = 0
var is_on_defence:bool
var is_on_caffe_machine:bool = false
var is_drinking_coffe:bool = false
var current_prop:Node2D
var pickable_item:Node2D

var _invulnerable_time = 1
var _invulnerable = 0

# on ready
onready var body = $body
onready var head = $head
onready var animation = $animation
onready var ray = $ray

onready var punch_left = $body/punch_left_position/punch
onready var punch_left_position = $body/punch_left_position
onready var punch_left_original_position = punch_left_position.position
onready var def_punch_left = $body/def_punch_left_position
onready var punch_right = $body/punch_right_position/punch
onready var punch_right_position = $body/punch_right_position
onready var punch_right_original_position = punch_right_position.position
onready var def_punch_right = $body/def_punch_right_position

onready var grabbed_anchor = $body/grabbed_anchor

onready var hit_sound = $hit_sound

# ------------- PUBLIC METHODS ----------------
func decrease_stamina(amount) -> void:
	if(is_on_defence || _invulnerable > 0): return
	_invulnerable = _invulnerable_time
	if(current_stamina > 0):
		current_stamina -= amount
	if(current_stamina <= 0):
		GameManager.end_game()
		emit_signal("on_die")
		return
	hit_sound.play_random_hit()
	_blink()
	emit_signal("on_stamina_change", current_stamina)

func get_stamina() -> float:
	return current_stamina
# ------------- ./PUBLIC METHODS --------------

func _ready():
	GameManager.register_player(self)
	emit_signal("on_stamina_change", current_stamina)
	
func _init():
	current_stamina = max_stamina

func _physics_process(delta: float) -> void:
	if(!is_drinking_coffe && !is_on_defence):
		if (dodge_cooldown < 0): 
			animation.play("idle")
			_handle_input()
		_apply_movement()
		_handle_attack(delta)
	_flip_sprite()
	
func _process(delta: float) -> void:
	if !_handle_defence():
		!_handle_dodge(delta)
	_handle_interaction()
	if _invulnerable > 0: _invulnerable -= delta
	
func _handle_interaction():
	var interaction = Input.is_action_just_pressed("interact")
	if !interaction: return
	if is_on_caffe_machine:
		_drink_coffe()
		return
	elif(pickable_item != null):
		current_prop = pickable_item
		current_prop.get_parent().remove_child(current_prop)
		grabbed_anchor.add_child(current_prop)
		current_prop.position = Vector2.ZERO
		current_prop.picked()
		pickable_item = null
	elif(current_prop != null):
		# drop
		current_prop = null
	
func _handle_input():
	move_direction = Vector2.ZERO
	move_direction.x = -int(Input.is_action_pressed("move_left")) + int(Input.is_action_pressed("move_right"))
	move_direction.y = -int(Input.is_action_pressed("move_up")) + int(Input.is_action_pressed("move_down"))
	desired_velocity = move_direction.normalized() * move_speed / (2 if current_prop != null else 1)

func _apply_movement():
	velocity = velocity.linear_interpolate(desired_velocity, _get_move_weight())
	velocity = move_and_slide(velocity)
	
func _get_move_weight() -> float:
	return 0.1

func _flip_sprite():
	var mouse_position = get_global_mouse_position()
	body.set_scale(Vector2(1 if mouse_position.x >= position.x else -1, 1))
	head.set_scale(Vector2(1 if mouse_position.x >= position.x else -1, 1))

func _handle_attack(delta) -> void:
	if Input.is_action_pressed("attack") && cooldown <= 0:
		cooldown = fire_rate * (2 if current_prop != null else 1)
		_cast_hit()
		_attack()
	cooldown -= delta

func _attack():
	if punching_direction == 1:
		punch_right.attack()
	else:
		punch_left.attack()
	punching_direction *= -1
	
func _handle_defence() -> bool:
	if Input.is_action_pressed("defence"):
		punch_left_position.position = def_punch_left.position
		punch_right_position.position = def_punch_right.position
		is_on_defence = true;
	else:
		punch_left_position.position = punch_left_original_position
		punch_right_position.position = punch_right_original_position
		is_on_defence = false
	return is_on_defence
	
func _handle_dodge(delta: float) -> bool:
	if Input.is_action_pressed("dodge") && dodge_cooldown < 0:
		_dodge()
		dodge_cooldown = dodge_rate
		return true
	dodge_cooldown -= delta
	return false

func _dodge():
	animation.play("evade" if body.scale.x > 0 else "evade_flip")
	var mouse_position = get_global_mouse_position()
	var angle = get_angle_to(mouse_position)
	move_direction = Vector2(cos(angle), sin(angle))
	var dodge_force = GameManager._rng.randi_range(200, 500)
	velocity = Vector2.ZERO
	velocity = velocity.linear_interpolate(move_direction * dodge_force, 1)
	velocity = move_and_slide(velocity)
	
func _cast_hit():
	# todo move out from here
	var mouse_position = get_global_mouse_position()
	var angle = get_angle_to(mouse_position)
	ray.cast_to = Vector2(cos(angle), sin(angle)) * 50
	ray.force_raycast_update()
	if ray.is_colliding():
		ray.get_collider().hit(10, position)

func ready_for_pick(prop: Node2D):
	pickable_item = prop

func not_ready_for_pick():
	pickable_item = null

func has_goal_item():
	return current_prop != null

func drop():
	if has_goal_item():
		current_prop.destroy()
		current_prop = null
	
func set_is_on_caffe_machine(value:bool):
	is_on_caffe_machine = value

func _drink_coffe():
	is_drinking_coffe = true
	animation.play("drink_coffe")
	yield(get_tree().create_timer(10, false), "timeout")
	is_drinking_coffe = false
	
func _coffe_drinked():
	if (current_stamina == max_stamina): return
	current_stamina += 5
	current_stamina = clamp(current_stamina, 0, max_stamina)
	emit_signal("on_stamina_change", current_stamina)
	

func _blink():
	body.get_node("sprite").modulate = Color(1, 0, 0)
	head.get_node("sprite").modulate = Color(1, 0, 0)
	yield (get_tree().create_timer(0.1, false), "timeout")
	body.get_node("sprite").modulate = Color(1, 1, 1)
	head.get_node("sprite").modulate = Color(1, 1, 1)

func get_grabbed_item(): return current_prop
