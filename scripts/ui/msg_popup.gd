extends Control

var _msg: Label
var _speaker: Label

func _ready():
	_msg = $MarginContainer/Panel/VBoxContainer/message
	_speaker = $MarginContainer/Panel/VBoxContainer/speaker

func set_message(from, value):
	_speaker.text = "%s:" % from
	_msg.text = value
