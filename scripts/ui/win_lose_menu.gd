extends PanelContainer


func _ready() -> void:
	GameManager.connect("round_ended", self, "_round_ended")
	GameManager.connect("round_started", self, "_round_started")
	
func _round_ended():
	show()

func _round_started():
	hide()


func _on_restart_button_pressed() -> void:
	GameManager.new_game()


func _on_quit_button_pressed() -> void:
	get_tree().quit()
