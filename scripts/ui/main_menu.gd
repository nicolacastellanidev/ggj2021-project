extends MarginContainer

var new_button
var quit_button

func _ready():
	GameManager.register_main_menu(self)

func _on_start_pressed():
	GameManager.new_game()

func _on_quit_pressed():
	get_tree().quit()
