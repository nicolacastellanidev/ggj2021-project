extends Node
class_name StateMachine

var _states = {}
var _old_state_index = null
var _current_state_index = null
var _current_state = null


func _init() -> void:
	set_process(false)
	set_physics_process(false)
	set_process_input(false)
	set_process_unhandled_input(false)


func _process(delta: float) -> void:
	if(_current_state != null):
		var next = _current_state.run(delta)
		if(next != null):
			go_to(next)


func add_state(index, gdclass, node_name) -> void:
	var state = gdclass.new()
	state.name = "%s_state" % node_name
	add_child(state)
	_states[index] = state


func go_to(new_state) -> void:
	var next = _states.get(new_state)
	assert(next != null)

	if _current_state != null:
		_current_state.exit()
	
	_old_state_index = _current_state_index
	_current_state_index = new_state
	
	next.enter(_old_state_index)
	_current_state = next


func start(first_state):
	set_process(true)
	go_to(first_state)
