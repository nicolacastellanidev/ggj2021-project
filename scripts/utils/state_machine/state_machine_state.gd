extends Node2D
class_name StateMachineState

func enter(old_state = null) -> void:
	pass

func run(delta: float):
	pass

func exit() -> void:
	pass
