extends Node
class_name Constants

enum GameStates { START, IDLE, NEW_REQUEST, EXEC_REQUESTS, COMPLETE_REQUEST, FAIL_REQUEST, PAUSE, END }

enum RoomTypes { OFFICE, BREAK_AREA, THEME_1, THEME_2, THEME_3, THEME_4 }
enum ItemCategories { THEME_1, THEME_2, THEME_3, THEME_4 }

enum DoorTypes { LEFT, TOP, RIGHT, BOTTOM }

const Input_PAUSE = "pause"

const Time_WORKING_HOURS: int = 8 #wmin #TODO - Final value: 8
const Time_WMIN_TO_RSEC: float = 1.0 #rsec
const Time_INITIAL_COUNTDOWN: int = 2 #rsec
const Time_FIRST_REQUEST_DELAY_MIN: int = 1 #rsec
const Time_FIRST_REQUEST_DELAY_MAX: int = 3 #rsec
const Time_NEXT_REQUEST_DELAY_MIN: int = 100 #rsec
const Time_NEXT_REQUEST_DELAY_MAX: int = 250 #rsec
const Time_TIMEOUT_REQUEST_MIN: int = 500 #rsec
const Time_TIMEOUT_REQUEST_MAX: int = 1500 #rsec

const Points_COMPLETED = 100
const Points_FAILED = -50

class ItemRequest:
	var id: int
	var timeout_timer: int
	var item
	var reward: int

class Room:
	var id
	var name
	var items_category
	var left_door
	var top_door
	var right_door
	var bottom_door
	var instance

class Message:
	var msg_id
	var speaker_id
	var text

class Character:
	var id
	var full_name

enum EnemyStates { PATROL, ENGAGE }
enum Messages { REQUEST_COMPLETED, REQUEST_FAILED, REQUEST_TIMEOUT, WELCOME_1, WELCOME_2, WELCOME_3, WELCOME_4, WELCOME_5, WELCOME_6, WELCOME_7 }
enum Conversations { WELCOME }
enum Characters { PLAYER, BOSS, CUSTOMER }
