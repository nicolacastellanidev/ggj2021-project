extends Control

export (Array, Texture) var pickable_textures

onready var stamina_number : Label = $MarginContainer/HBoxContainer/BARS/STAMINA/STAMINAmargin/BCKGRND/Number 
onready var timer_number : Label= $MarginContainer/HBoxContainer/BARS/TIMER/TIMERmargin/BCKGRND/Number

onready var stamina_bar : TextureProgress = $MarginContainer/HBoxContainer/BARS/STAMINA/stamina_bar
onready var timer_bar : TextureProgress = $MarginContainer/HBoxContainer/BARS/TIMER/timer_bar
onready var tween : Tween = $MarginContainer/HBoxContainer/BARS/Tween

onready var current_item_sprite: TextureRect = $MarginContainer/HBoxContainer/PICKUP/current_item_sprite
onready var score_label: Label = $MarginContainer/HBoxContainer/BARS/SCORE/score_label

var paused_timer = false

func set_MaxTimer(new_max_value):
	timer_bar.max_value = new_max_value;
	update_Timer(new_max_value)

func set_MaxStamina(new_max_value):
	stamina_bar.max_value =new_max_value;
	update_Stamina(new_max_value)

func _ready():
	GameManager.register_hud(self)
	GameManager.connect("request_accepted", self, "update_current_item_sprite")
	GameManager.connect("points_changed", self, "update_score")
	GameManager.connect("round_ended", self, "pause_timer")
	GameManager.connect("round_started", self, "start_timer")
	
func _process(delta: float) -> void:
	if paused_timer: return
	if (GameManager == null): return
	update_Timer(int(GameManager.get_remaining_time()))
	
func pause_timer():
	paused_timer = true
func start_timer():
	paused_timer = false

func update_Timer(new_value):
	timer_number.text = str(new_value/60)+":"+str(new_value%60);
	timer_bar.value = new_value;

func update_Stamina(new_value):
	if stamina_number == null:
		stamina_number = $MarginContainer/HBoxContainer/BARS/STAMINA/STAMINAmargin/BCKGRND/Number
	stamina_number.text = str(new_value);
	if stamina_bar == null:
		stamina_bar = $MarginContainer/HBoxContainer/BARS/STAMINA/stamina_bar
	stamina_bar.value = new_value;


func _on_player_stamina_change(player_health):
	update_Stamina(player_health)
	
func update_current_item_sprite():
	var request = GameManager._requests[GameManager._current_request_id]
	if request:
		match request.item:
			Constants.ItemCategories.THEME_1:
				current_item_sprite.texture = pickable_textures[0]
			Constants.ItemCategories.THEME_2:
				current_item_sprite.texture = pickable_textures[1]
			Constants.ItemCategories.THEME_3:
				current_item_sprite.texture = pickable_textures[2]
			Constants.ItemCategories.THEME_4:
				current_item_sprite.texture = pickable_textures[3]

func update_score(score):
	score_label.text = str(score)
