extends Node2D

onready var ui_interact = $ui_interact

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	ui_interact.visible = false


func _on_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		ui_interact.visible = true;
		body.set_is_on_caffe_machine(true)

func _on_body_exited(body: Node) -> void:
	if body.is_in_group("player"):
		ui_interact.visible = false
		body.set_is_on_caffe_machine(false)
