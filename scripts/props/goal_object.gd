extends Node2D

export (Array, Texture) var pickable_textures

onready var pick_sprite = $ui_interact
onready var graphics = $body/graphics

export var category_id: int = 0
var _category = null


var picked:bool = false
var destroyed = false

func _ready():
	pick_sprite.visible = false
	update_current_item_sprite()
	# _category = Constants.ItemCategories.values()[category_id]

func set_item_category(value):
	_category = value

func destroy():
	graphics.visible = false
	destroyed = true
	queue_free()

func picked():
	pick_sprite.visible = false
	
func flip(direction: int):
	if destroyed: return
	if (direction > 0 && graphics.scale.x < 0 || direction < 0 && graphics.scale.x >= 0):
		graphics.set_scale(Vector2(-graphics.scale.x, graphics.scale.y))

func _on_body_entered(body: Node) -> void:
	if destroyed: return
	if picked: return
	if body.is_in_group("player"):
		pick_sprite.visible = true;
		body.ready_for_pick(self)

func _on_body_exited(body: Node2D) -> void:
	if destroyed: return
	if body.is_in_group("player"):
		pick_sprite.visible = false;
		body.not_ready_for_pick()
		
func update_current_item_sprite():
	match _category:
		Constants.ItemCategories.THEME_1:
			graphics.texture = pickable_textures[0]
		Constants.ItemCategories.THEME_2:
			graphics.texture = pickable_textures[1]
		Constants.ItemCategories.THEME_3:
			graphics.texture = pickable_textures[2]
		Constants.ItemCategories.THEME_4:
			graphics.texture = pickable_textures[3]
