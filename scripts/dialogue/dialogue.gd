extends Node
class_name DialogueManager

var _is_open: bool = false

var _messages: Dictionary
var _conversations: Dictionary
var _speakers: Dictionary

var _ui

var _current_conversation = null
var _current_conversation_index: int = -1


func _ready():
	_ui = get_node("/root/main/ui/msg_popup")
	if _ui != null:
		_ui.hide()
	
	_add_speaker(Constants.Characters.PLAYER, "Me")
	_add_speaker(Constants.Characters.BOSS, "Didacus")
	_add_speaker(Constants.Characters.CUSTOMER, "Customer")
	
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_1, "Hello Dan!! Are you ready? Today is your first day at our Lost & Found office!\n\n(Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_2, "Stay sharp, answer to my clients, and get back the stuff.\n\n(Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_3, "Use [W,A,S,D] keys to move around office and magazine. But take care in there.\n\n(Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_4, "Use [LEFT MOUSE BUTTON] to attack, [RIGHT MOUSE BUTTON] to dodge. (Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_5, "Use [E] to interact, you can grab the desired object and ungrab at any time you want.\n\n(Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_6, "And now go! Take the stuff and come back! And remember, losing stamina will make you lose the game.\n\n(Press SPACEBAR)")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.WELCOME_7, "Remember to come back here and drik COFFE regularly!\n\n(Press SPACEBAR)")
	
	_add_msg(Constants.Characters.CUSTOMER, Constants.Messages.REQUEST_COMPLETED, "Thank you!")
	_add_msg(Constants.Characters.CUSTOMER, Constants.Messages.REQUEST_FAILED, "It is the wrong item!!")
	_add_msg(Constants.Characters.BOSS, Constants.Messages.REQUEST_TIMEOUT, "The customer is gone away. Come on, you are too slow slow!")
	
	_add_conversation(Constants.Conversations.WELCOME, [ Constants.Messages.WELCOME_1, Constants.Messages.WELCOME_2, Constants.Messages.WELCOME_3, Constants.Messages.WELCOME_4, Constants.Messages.WELCOME_5, Constants.Messages.WELCOME_6, Constants.Messages.WELCOME_7 ])	

func _add_speaker(id, name):
	var speaker = Constants.Character.new()
	speaker.id = id
	speaker.full_name = name
	_speakers[id] = speaker

func _add_msg(speaker_id, msg_id, text):
	var msg = Constants.Message.new()
	msg.msg_id = msg_id
	msg.speaker_id = speaker_id
	msg.text = text
	_messages[msg_id] = msg

func _add_conversation(id, msg_ids: Array):
	var conversation = msg_ids
	_conversations[id] = msg_ids


func _input(event):
	if _is_open and Input.is_action_just_pressed("ui_select"):
		close_message()


func open_message(id):
	if _is_open:
		return

	var msg = _messages[id]
	assert(msg != null)
	
	var speaker = _speakers[msg.speaker_id]
	assert(speaker != null)
	
	print("[DIALOGUE] %s: %s" % [speaker.full_name, msg.text])
	_is_open = true
	_ui.set_message(speaker.full_name, msg.text)
	_ui.show()


func close_message():
	if !_is_open:
		return
	
	_is_open = false
	if(_current_conversation != null):
		_current_conversation_index = _current_conversation_index + 1
		if(_current_conversation_index < _current_conversation.size()):
			open_message(_current_conversation[_current_conversation_index])
			return
		else:
			_current_conversation = null
			_current_conversation_index = -1
	
	_ui.hide()


func start_conversation(id):
	if _is_open:
		return

	var conversation = _conversations[id]
	assert(conversation != null)
	
	_current_conversation = conversation
	_current_conversation_index = 0
	
	open_message(conversation[0])
