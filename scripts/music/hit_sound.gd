extends Node2D

export (Array, AudioStream) var hit_sounds = []

onready var stream: AudioStreamPlayer2D = $stream

func _ready():
	randomize()
	# _test_sounds()
	
func _test_sounds():
	play_random_hit()

func play_random_hit():
	_set_stream(hit_sounds[randi() % hit_sounds.size()])
	
func _set_stream(nextStream: AudioStream):
	if stream.playing:
		stream.stop()
	stream.set_stream(nextStream)
	stream.play()
