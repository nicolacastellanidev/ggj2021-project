extends Node2D
export var is_dungeon = false
export var office_music: 	AudioStream
export var dungeon_music: 	AudioStream

onready var stream: AudioStreamPlayer2D = $stream

func _ready():
	if(is_dungeon):
		play_dungeon_music()

func play_office_music():
	_set_stream(office_music)
	
func play_dungeon_music():
	_set_stream(dungeon_music)
	
func _set_stream(nextStream: AudioStream):
	if stream.playing:
		stream.stop()
	stream.set_stream(nextStream)
	stream.play()
