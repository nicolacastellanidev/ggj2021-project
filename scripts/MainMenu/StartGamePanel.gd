extends Panel

var world_scene = preload("res://scenes/World.tscn")
var GUIpanel_scene = preload("res://scenes/GUI/GUI.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Button").connect("pressed",self,"_on_Button_pressed")

	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	get_tree().change_scene_to(world_scene)
	var GUIpanel_node = GUIpanel_scene.instance()
	pass # Replace with function body.
