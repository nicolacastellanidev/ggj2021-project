extends BaseRoom

export var size_x = 20
export var size_y = 20

var navigator : Navigation2D
var map : TileMap
var enemies: Node
var player : KinematicBody2D
var l_enter : Position2D
var r_enter : Position2D
var u_enter : Position2D
var low_enter : Position2D

var _room_id
var _item_category_id

var screen_size
enum {TOP, WALL, GROUND}

var enter_door_cooldown = 0

func _process(delta):
	enter_door_cooldown -= delta

func setup(room_id, item_category_id):
	_room_id = room_id
	_item_category_id = item_category_id
	
	screen_size = Vector2(320, 320) #get_viewport_rect().size
	navigator = $World/Nav
	map = $World/Nav/Map
	enemies = $World/Enemise
	player = GameManager._player
	l_enter = $World/left_enter
	r_enter = $World/right_enter
	u_enter = $World/upper_enter
	low_enter = $World/lower_enter
	map = $World/Nav/Map
	
	create_walls()
	
#	yield(get_tree().create_timer(0.5), "timeout")
	
	spawn_items()
	reload()


func reload():
	if enemies.get_children().size() >= 0:
		for i in enemies.get_children():
			#remove_child(i)
			i.queue_free()
	
	spwan_enemies()


func create_walls():
	for i in range(size_x):
		for j in range(size_y):
			#left top
			if i == 0 and j < size_y-3 and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,TOP)
			#upper top  
			if j == 0  and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,TOP)
			#right top
			if i == size_x-1 and j < size_y-3  and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,TOP)
			#lower top
			if j == size_y-3  and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,TOP)
			#construction of lower wall
			if j >size_y-3  and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,WALL)
			#construction of upper wall
			if (j >0 and j<3)  and map.get_cell(i,j) == map.INVALID_CELL:
				map.set_cell(i,j,WALL)
			#terrain
			else:
				if map.get_cell(i,j) == map.INVALID_CELL:
					map.set_cell(i,j,GROUND)


const enemy_array = [
	preload("res://scenes/Enemies/Dragon.tscn"), 
	preload("res://scenes/Enemies/Guitar.tscn"),
	preload("res://scenes/Enemies/Smartphone.tscn"),
	preload("res://scenes/Enemies/Umbrella.tscn"),
	preload("res://scenes/Enemies/WashingMachine.tscn")
]
func spwan_enemies():
	var number = GameManager._rng.randi_range(2,10)
	for i in range(number):
		var x_enemy= GameManager._rng.randf_range(screen_size.x*2/size_x * 5, screen_size.x*2 - screen_size.x*2/size_x * 5)
		var y_enemy= GameManager._rng.randf_range(screen_size.y*2/size_y * 5, screen_size.y*2 - screen_size.y*2/size_y * 5)
		var enemy_instance = enemy_array[GameManager._rng.randi_range(0, enemy_array.size() - 1)].instance()
		enemies.add_child(enemy_instance)
		enemy_instance.position = Vector2(x_enemy,y_enemy)

#spawn of the object to grab
var objective = preload("res://scenes/pickups/goal_object.tscn")
func spawn_items():
	var x_objective = GameManager._rng.randf_range( screen_size.x/size_x *5, screen_size.x - screen_size.x/size_x *5)
	var y_objective = GameManager._rng.randf_range( screen_size.y/size_y *5, screen_size.y - screen_size.y/size_y *5)
	#adding the sapwn to the surface
	var objective_instance = objective.instance()
	objective_instance.set_item_category(_item_category_id)
	add_child(objective_instance)
	#setting the positin
	objective_instance.position = Vector2(x_objective,y_objective)


func on_room_enter(door = Constants.DoorTypes.TOP):
	match door:
		Constants.DoorTypes.LEFT:
			player.position = l_enter.position
		
		Constants.DoorTypes.TOP:
			player.position = u_enter.position
		
		Constants.DoorTypes.RIGHT:
			player.position = r_enter.position
		
		Constants.DoorTypes.BOTTOM:
			player.position = low_enter.position
	
	reload()


func _on_left_door_enter():
	if (enter_door_cooldown > 0): return
	enter_door_cooldown = 1
	GameManager.change_room(_room_id, Constants.DoorTypes.LEFT)

func _on_right_door_enter():
	if (enter_door_cooldown > 0): return
	enter_door_cooldown = 1
	GameManager.change_room(_room_id, Constants.DoorTypes.RIGHT)

func _on_upper_door_enter():
	if (enter_door_cooldown > 0): return
	enter_door_cooldown = 1
	GameManager.change_room(_room_id, Constants.DoorTypes.TOP)

func _on_lower_door_enter():
	if (enter_door_cooldown > 0): return
	enter_door_cooldown = 1
	GameManager.change_room(_room_id, Constants.DoorTypes.BOTTOM)
