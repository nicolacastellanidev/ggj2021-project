extends BaseRoom

var player_spawn_position = null

func on_room_enter(door = Constants.DoorTypes.TOP):
	if (player_spawn_position == null):
		player_spawn_position = $root/player_respawn_point
	match door:
		Constants.DoorTypes.TOP:
			GameManager._player.global_position = player_spawn_position.global_position
		_:
			GameManager._player.global_position = player_spawn_position.global_position
			

func _on_office_door_enter() -> void:
	GameManager.change_room(Constants.RoomTypes.OFFICE, Constants.DoorTypes.TOP)


func _on_player_drop(item) -> void:
	#GameManager.give_item(Constants.ItemCategories.values()[item.category_id])
	var item_category = item._category
	GameManager.give_item(item_category)
