extends Node2D

signal door_enter # if the door is hit the signal is emmited 


func _ready():
	pass # Replace with function body.


func _on_body_entered(body):
	if body.is_in_group("player"):
		emit_signal("door_enter")
