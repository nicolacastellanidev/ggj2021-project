extends KinematicBody2D

# public vars
export var health:float = 100.0
export var max_health:float = 100.0
export var range_of_engage: float = 120.0
export var moveSpeed: float = 25.0
export var fire_rate: float = 2.0
export var damage: float = 5

export var bullet_sprite: Texture

#private vars
var _sm: StateMachine
var velocity = Vector2.ZERO
var bool_player = null
var distance: Vector2
var cooldown: float = 0.0
var bool_dead = false
var color_sprite: Color
var green: Color = Color(0.0, 1.0, 0.0)
var bullet

# on ready
onready var fire_point = $fire_point
onready var sprite = $animated_sprite
onready var tween = $tween
onready var hit_sound = $hit_sound

func _init() -> void:
	_sm = StateMachine.new()
	add_child(_sm)

func _ready() -> void:
	bullet = preload("res://scenes/Enemies/Bullet.tscn")
	color_sprite = sprite.modulate
	_sm.add_state(Constants.EnemyStates.PATROL, PatrolState, "patrol")
	_sm.add_state(Constants.EnemyStates.ENGAGE, EngageState, "engage")
	
	_sm.start(Constants.EnemyStates.PATROL)

func _physics_process(delta) -> void:
	velocity = Vector2.ZERO
	
	range_and_attack()
	cooldown -= delta

func range_and_attack():
	if bool_dead: return
	
	if bool_player != null:
		distance = global_position - bool_player.global_position 
		if distance.length() < range_of_engage:
			if cooldown > 0: return
			cooldown = fire_rate
			blink_attack()
			yield (get_tree().create_timer(1.0, false), "timeout")
			if is_in_group("melee"):
				stomp(bool_player)
			elif is_in_group("ranged"):
				fire(-distance, bool_player.velocity)
		else:
			velocity = position.direction_to(bool_player.global_position) * moveSpeed
			velocity = move_and_slide(velocity)

func fire(playerDistance: Vector2, playerVelocity: Vector2):
	var bullet_instance = bullet.instance()
	get_parent().add_child(bullet_instance)

	bullet_instance.global_position = fire_point.global_position
	bullet_instance.set_direction(playerDistance + playerVelocity / 2)
	bullet_instance.set_texture(bullet_sprite)

func stomp(body: Node):
	if (tween.is_active()): tween.stop_all()
	tween.interpolate_property(self, "position",
	position, position + Vector2(
		20 if body.position.x > global_position.x else -20, 
		20 if body.position.y > global_position.y else -20
	), .25,
	Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.start()

func hit(damage, colliderPosition):
	health -= damage
	if health > 0:
		_blink()
		_knock_back(colliderPosition)
		hit_sound.play_random_hit()
	else:
		tween.stop_all()
		sprite.modulate = Color(0.25, 0.25, 0.25)
		bool_dead = true

func blink_attack():
	sprite.modulate = green.lightened(0.01)
	yield (get_tree().create_timer(1.0, false), "timeout")
	sprite.modulate = color_sprite

func _blink():
	sprite.modulate = Color(1, 0, 0)
	yield (get_tree().create_timer(0.1, false), "timeout")
	sprite.modulate = color_sprite

func _knock_back(colliderPosition: Vector2):
	if (tween.is_active()): tween.stop_all()
	tween.interpolate_property(self, "position",
	position, position + Vector2(
		-20 if colliderPosition.x > global_position.x else 20, 
		-20 if colliderPosition.y > global_position.y else 20
	), .25,
	Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.start()

func _on_PlayerDetectionZone_body_entered(body: Node):
	if body.is_in_group("player"):
		bool_player = body
		_sm.go_to(Constants.EnemyStates.ENGAGE)

func _on_tween_all_completed() -> void:
	tween.stop_all()


func _on_hitbox_body_entered(body: Node):
	if body.is_in_group("player"):
		body.decrease_stamina(damage)
