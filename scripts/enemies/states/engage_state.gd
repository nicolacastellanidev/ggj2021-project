extends StateMachineState
class_name EngageState

func enter(old_state = null) -> void:
	print("[ENGAGE STATE] Enter")

func run(delta: float):
	#print("[ENGAGE STATE] Run")
	return null

func exit() -> void:
	print("[ENGAGE STATE] Exit")
