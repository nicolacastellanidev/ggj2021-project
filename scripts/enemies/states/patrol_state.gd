extends StateMachineState
class_name PatrolState

func enter(old_state = null) -> void:
	print("[PATROL STATE] Enter")

func run(delta: float):
	#print("[PATROL STATE] Run")
	return null

func exit() -> void:
	print("[PATROL STATE] Exit")
