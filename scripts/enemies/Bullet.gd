extends Area2D

# public vars
export (int) var speed = 2
export var damage: float = 5

# private vars
var direction := Vector2.ZERO

func _physics_process(delta) -> void:
	var velocity = direction * speed * delta

	global_position += velocity

func set_direction(direction: Vector2):
	self.direction = direction.normalized() * 50


func _on_Bullet_body_entered(body: Node) ->void:
	if body.is_in_group("player"):
		body.decrease_stamina(damage)
	queue_free()

func set_texture(texture: Texture):
	$Sprite.texture = texture
